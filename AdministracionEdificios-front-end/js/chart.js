var gastoMeses = document.getElementById('gastoMeses').getContext('2d');
var myChart = new Chart(gastoMeses, {
    type: 'bar',
    data: {
        labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dic'],
        datasets: [{
            label: '$',
            data: [7.245, 8.124, 7.842, 7.543, 7.554, 8.242, 8.532, 7.543, 7.845, 7.543, 7.245, 7.243, ],
            backgroundColor: [
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff'
            ],
            borderColor: [
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff',
                '#007bff'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true // que arranque en cero o no
                }
            }],
            xAxes: [{ barPercentage: 0.5 }] // ancho de las barras
        },
        legend: {
            display: false
        }
    }
});

var gasto = document.getElementById('gasto').getContext('2d');
var myDoughnutChart = new Chart(gasto, {
    type: 'doughnut',
    data: {
        labels: ['Consumos / Tributos', 'Servicios / Abonos', 'Calefacción', 'Varios', 'Administración'],
        datasets: [{
            label: '%',
            data: [13, 64, 16, 2, 3],
            backgroundColor: [
                '#007bff',
                '#007bffc7',
                '#007bff9e',
                '#007bff70',
                '#007bff2e'
            ],
            borderColor: [
                'white',
                'white',
                'white',
                'white',
                'white',
            ],
            borderWidth: 1,
        }]
    },
    options: {
        maintainAspectRatio: false,
        aspectRatio: 1,
        cutoutPercentage: 70, // ancho 
        legend: {
            display: true,
            position: 'bottom',
            labels: {
                fontSize: 10, //point style's size is based on font style not boxed width.
                usePointStyle: true,
            }
        }
    }

});